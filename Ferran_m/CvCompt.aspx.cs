﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using System.Web.Mail;
using System.Configuration;
using System.Net.Mail;
using System.Net;

public partial class CvCompt : System.Web.UI.Page
{
    private string SelectedSkills;
    System.Net.Mail.MailMessage myemail = new System.Net.Mail.MailMessage();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Application["IdVisitor"] != null)
            HelloLabel.Text = Application["IdVisitor"].ToString();
        else
            HelloLabel.Text = "UnknownUser";
    }
    protected void ContinueForCV_Click(object sender, EventArgs e)
    {
        ContinueForCV.Visible = false;
        CheckBoxCompt.Visible = true;
        ButtonconfirmSkills.Visible = true;
        Tabpres.Visible = true;
    }

    public string SendMail(string content)
{
    MailAddress from = new MailAddress("morgan.ferrandis@gmail.com");
    MailAddress to = new MailAddress("morgane.ferrandis@epitech.eu");
    string passwd = "pSrcoobC";
    MailMessage email = new MailMessage(from, to);
    SmtpClient sc = new SmtpClient("smtp.gmail.com");

    email.Subject = "Une visite en plus !";
    email.IsBodyHtml = false;
    email.Body = Application["IdVisitor"] +" est passé sur ta page car il recherche les compétences suivantes: " + content + " de la part de ";
    sc.EnableSsl = true;
    sc.Port = 587;
    sc.Host = "smtp.gmail.com";
    sc.Credentials = new System.Net.NetworkCredential(from.Address, passwd);
        try
        {
            sc.Send(email);
            return (null);
        }
            catch (Exception ex)
        {
                return (ex.ToString());
        }
    }
    protected void ButtonconfirmSkills_Click(object sender, EventArgs e)
    {
        foreach (ListItem item in CheckBoxCompt.Items)
        {
            if (item.Selected)
            {
                SelectedSkills += (item.Value + " ");
            }
        }
        if((skillresponse.Text = SendMail(SelectedSkills)) != null)
            skillresponse.Visible = true;
    }
}