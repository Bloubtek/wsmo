﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CvCompt.aspx.cs" Inherits="CvCompt" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>CV et compétences</title>
        <link rel="stylesheet" type="text/css" href="Styles/StyleCVPage.css" />
</head>
<body>
    <form id="form1" runat="server" style="margin-left:500px; margin-right:500px; width:1000px; height:initial; background-color:#464a45;">
    <asp:HyperLink ID="HyperLink1" NavigateUrl="~/Default.aspx" runat="server">retour</asp:HyperLink>
        <div>
            <h1>Discover my skills !
                <br />
            </h1>
            <div>
            <h3>Hello dear <asp:Label runat="server" ID="HelloLabel"></asp:Label> i'm gonna explain you the rules of the house. As a recruiter, you're looking for some skills from your intern and of course, you will be able to choose the skills you want from me !
            <br />
                There will be a list of my skills right below this, where you can choose what skills you're looking for.
            <br />
                So let's start !
            <br />
            </h3>
            <h2>
                <asp:Button ID="ContinueForCV" runat="server" Text="Continue" OnClick="ContinueForCV_Click" />
                <br />
            </h2>
                </div>
            <div id="CheckboxDiv" aria-orientation="vertical" style="vertical-align: middle; align-self: center;">
                <asp:table runat="server" ID="Tabpres" Visible="false">
                    <asp:TableRow>
                        <asp:TableCell BorderColor="Black" BorderStyle="Double" Width="150" VerticalAlign="Middle">Compétences</asp:TableCell>
                        <asp:TableCell BorderColor="Black" BorderStyle="Double" Width="100" VerticalAlign="Middle">Experience</asp:TableCell>
                    </asp:TableRow>
                </asp:table>
                <asp:CheckBoxList ID="CheckBoxCompt" Visible="false" runat="server" Width="150">
                    <asp:ListItem>C#/.NET</asp:ListItem>
                    <asp:ListItem>Xaml</asp:ListItem>
                    <asp:ListItem>WPF</asp:ListItem>
                    <asp:ListItem>ASP .Net</asp:ListItem>
                    <asp:ListItem>C++</asp:ListItem>
                    <asp:ListItem>Java</asp:ListItem>
                    <asp:ListItem>Python</asp:ListItem>
                    <asp:ListItem>C</asp:ListItem>
                    <asp:ListItem>MySQL</asp:ListItem>
                    <asp:ListItem>Systèmes Unix</asp:ListItem>
                    <asp:ListItem>Windows</asp:ListItem>
                </asp:CheckBoxList>
            </div>
            <h2>
                <asp:Button ID="ButtonconfirmSkills" runat="server" Text="Confirm" OnClick="ButtonconfirmSkills_Click" Visible="false" />
                <asp:Label ID="skillresponse" runat="server" Visible="false" />
            </h2>
        </div>
    </form>
</body>
</html>
