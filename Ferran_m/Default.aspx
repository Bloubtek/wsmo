﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title aria-valuetext="16"> Welcome on the dev site.</title>
    <link rel="stylesheet" type="text/css" href="Styles/StyleMainPage.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        Bienvenue <asp:Label ID="responseLabel" runat="server" aria-readonly="true" title="" /><br/>
        Cette page a été faite en asp .net <br />
        <asp:TextBox ID="TextBox1" runat="server" OnTextChanged="TextBox1_TextChanged" TextMode="SingleLine" Text="Identifiant"></asp:TextBox>
        <asp:Button ID="Button1" runat="server" Text="Confirmer" OnClick="Button1_Click"/>

        <div aria-orientation="horizontal">
            <asp:Table ID="RootTab" runat="server">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:HyperLink ID="VersCv" runat="server" NavigateUrl="~/CvCompt.aspx">CV et compétences</asp:HyperLink></asp:TableCell>
                    <asp:TableCell>
                        <asp:HyperLink ID="VersContact" runat="server" NavigateUrl="~/Contact.aspx">Me contacter</asp:HyperLink></asp:TableCell>
                    <asp:TableCell>
                        <asp:HyperLink ID="VersParcours" runat="server" NavigateUrl="~/Parcours.aspx"> Mon parcours </asp:HyperLink></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </div>
    </div>
    </form>
</body>
</html>
