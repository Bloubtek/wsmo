﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Contact.aspx.cs" Inherits="Default2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>me contacter</title>
    <link rel="stylesheet" type="text/css" href="Styles/ContactSheet.css" />
</head>
<body>
    <form id="form1" runat="server">
            <asp:HyperLink ID="HyperLink1" NavigateUrl="~/Default.aspx" runat="server">retour</asp:HyperLink>
        <div>
            <h1>
                <asp:Table ID="ContactTab" runat="server" BorderStyle="Double">
                    <asp:TableRow>
                        <asp:TableCell>Téléphone: </asp:TableCell>
                        <asp:TableCell>06.60.13.13.57</asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>Email: </asp:TableCell>
                        <asp:TableCell>morgane.ferrandis@epitech.eu</asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>Linkedin: </asp:TableCell>
                        <asp:TableCell><asp:HyperLink runat="server" NavigateUrl="https://fr.linkedin.com/in/morgane-ferrandis-2a488492" >https://fr.linkedin.com/in/morgane-ferrandis-2a488492
</asp:HyperLink></asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </h1>
        </div>
    </form>
</body>
</html>
